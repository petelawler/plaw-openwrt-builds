#! /usr/bin/env python3

"""Module docstring.

A really long usage message should go here
"""

from datetime import datetime as dt
from sys import path

DEBUG = False
FILENAME = "openwrt_builder"
SQLALCHEMY_FILENAME = FILENAME + '.db'
TEXT_FILENAME = FILENAME + '.txt'
PICKLE_FILENAME = FILENAME + '.pkl'
RUNTIME = dt.now()
HTTP = "https"
OpenWRT_Release_Base_URL = HTTP + "://downloads.openwrt.org/releases/"
OpenWRT_Release_Version = "19.07.5"
BUILDER_EXECUTABLE = path[0] + "/" + "make-openwrt.sh -t images"
OpenWRT_CPU = "mips_24kc"  # mips_24kc, aarch64_cortex-a72 etc
OpenWRT_Target = "ath79"  # ar71xx, x86 etc
OpenWRT_Profile = "tl-wr1043nd-v1"  # tl-wr1043nd-v1, archer-c7-v1 etc.
OpenWRT_Packages = ""  # luci-app-adblock luci-app-privoxy etc
OpenWRT_Files = ""  # etc/config/openssl etc/openvpn/my_vpn.vpn etc.
OpenWRT_Type = "generic"  # tiny or generic
OpenWRT_Variant = "vanilla"  # SELF FREEFORM NAME
OpenWRT_Core_Packages = "coreutils kmod-zram screen wget zram-swap"
OpenWRT_Desirable_Packages = "luci-ssl ca-certificates ca-bundle zlib libopenssl"
OpenWRT_USBStorage_Packages = "blkid block-mount e2fsprogs kmod-fs-ext4 kmod-usb-ohci kmod-usb-storage kmod-usb-uhci kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport sfdisk"
OpenWRT_ExtraFileSystems_Packages = "kmod-fs-cramfs kmod-fs-jfs kmod-fs-vfat dosfstools"
OpenWRT_OpenVPN_Packages = "luci-app-openvpn openvpn-openssl openssl-util openvpn-easy-rsa"
OpenWRT_Privacy_Packages = "luci-app-adblock luci-app-privoxy luci-app-dnscrypt-proxy luci-app-https-dns-proxy luci-app-polipo luci-app-nextdns"
OpenWRT_IPS_Packages = "luci-app-noddos"
OpenWRT_Fwknop_Packages = "luci-app-fwknopd"
OpenWRT_Nifty_Packages = "bash luci-app-commands luci-app-mwan3 luci-app-statistics luci-app-wifischedule luci-app-wol rsync terminfo"
OpenWRT_CaptivePortal_Packages = "coova-chilli nodogsplash2 wifidog-tls"
OpenWRT_User_Packages = "sudo shadow-common shadow-groupadd shadow-useradd shadow-usermod"
OpenWRT_USB_Tethernet = "kmod-usb-net kmod-usb-net-rndis kmod-usb-net-cdc-ether"
OpenWRT_Coreutils_Simple_Packages = "coreutils"
OpenWRT_Coreutils_Packages = "coreutils coreutils-base64 coreutils-cat coreutils-chcon coreutils-chgrp coreutils-chmod coreutils-chown coreutils-chroot coreutils-cksum coreutils-comm coreutils-cp coreutils-csplit coreutils-date coreutils-dd coreutils-dircolors coreutils-echo coreutils-expand coreutils-factor coreutils-false coreutils-fmt coreutils-fold coreutils-hostid coreutils-install coreutils-join coreutils-kill coreutils-link coreutils-ln coreutils-logname coreutils-ls coreutils-mkdir coreutils-mknod coreutils-mktemp coreutils-mv coreutils-nice coreutils-nl coreutils-nohup coreutils-nproc coreutils-od coreutils-paste coreutils-pathchk coreutils-pinky coreutils-pr coreutils-printenv coreutils-ptx coreutils-pwd coreutils-realpath coreutils-rm coreutils-rmdir coreutils-runcon coreutils-sha1sum coreutils-sha224sum coreutils-sha384sum coreutils-sha512sum coreutils-shred coreutils-shuf coreutils-sleep coreutils-split coreutils-stat coreutils-stdbuf coreutils-stty coreutils-sum coreutils-sync coreutils-tac coreutils-timeout coreutils-touch coreutils-true coreutils-truncate coreutils-tsort coreutils-tty coreutils-uname coreutils-unexpand coreutils-unlink coreutils-users coreutils-vdir coreutils-who coreutils-whoami"
OpenWRT_Procpsng_Simple_Packages = "procps-ng"
OpenWRT_Procpsng_Packages = "procps-ng procps-ng-free procps-ng-kill procps-ng-pgrep procps-ng-pkill procps-ng-pmap procps-ng-ps procps-ng-pwdx procps-ng-skill procps-ng-slabtop procps-ng-snice procps-ng-tload procps-ng-top procps-ng-uptime procps-ng-vmstat procps-ng-w procps-ng-watch"
OpenWRT_User_Simple_Packages = "sudo shadow-common shadow-groupadd shadow-useradd shadow-usermod"
OpenWRT_User_Packages = "sudo shadow-chage shadow-chpasswd shadow-chfn shadow-chsh shadow-expiry shadow-faillog shadow-gpasswd shadow-groupadd shadow-groupdel shadow-groupmems shadow-groupmod shadow-groups shadow-lastlog shadow-login shadow-newgidmap shadow-newgrp shadow-newuidmap shadow-nologin shadow-passwd shadow-su shadow-useradd shadow-userdel shadow-usermod shadow-vipw"
OpenWRT_Shell = "bash"
OpenWRT_Stats_Simple_Packages = "luci-app-statistics"
OpenWRT_Stats_Packages = "luci-app-statistics collectd collectd-mod-conntrack collectd-mod-cpu collectd-mod-dns collectd-mod-entropy collectd-mod-interface collectd-mod-iptables collectd-mod-iwinfo collectd-mod-load collectd-mod-memory collectd-mod-netlink collectd-mod-network collectd-mod-openvpn collectd-mod-ping collectd-mod-processes collectd-mod-protocols collectd-mod-rrdtool collectd-mod-tcpconns collectd-mod-wireless"
OpenWRT_Image_Builder = "OpenWRT-imagebuilder-" + OpenWRT_Release_Version + "-" + OpenWRT_Target + "-" + OpenWRT_Type + ".Linux-x86_64"
OpenWRT_Image_Builder_Archive = OpenWRT_Image_Builder + ".tar.xz"
OpenWRT_Target_Release_URL = HTTP + "://downloads.OpenWRT.org/releases/" + OpenWRT_Release_Version + "/targets/" + OpenWRT_Target + "/" + OpenWRT_Type
OpenWRT_Target_Packages_URL = OpenWRT_Target_Release_URL + "/" + "packages"
OpenWRT_Target_Builder_URL = OpenWRT_Target_Release_URL + "/" + OpenWRT_Image_Builder_Archive
OpenWRT_Target_sha256sums = OpenWRT_Target_Release_URL + "/sha256sums"
OpenWRT_CPU_Packages_URL = HTTP + "://downloads.OpenWRT.org/releases/" + OpenWRT_Release_Version + "/packages/" + OpenWRT_CPU
OpenWRT_CPU_Packages_Feeds_URL = OpenWRT_CPU_Packages_URL + "/feeds.conf"

print (OpenWRT_CPU_Packages_Feeds_URL)
