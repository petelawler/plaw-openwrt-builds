#!/usr/bin/env python3
"""Module docstring.

A really long usage message should go here
"""

from datetime import datetime as dt
from pickle import dump, load, HIGHEST_PROTOCOL
from ssl import PROTOCOL_TLSv1_2, SSLContext
from shlex import split
from subprocess import check_call
from urllib import request

from bs4 import BeautifulSoup
from openwrt_builder_globals import BUILDER_EXECUTABLE, DEBUG, HTTP, RUNTIME, PICKLE_FILENAME
from openwrt_builder_globals import OpenWRT_Release_Version


def last_modified(URL):
    https_tls_handler = request.HTTPSHandler(context=SSLContext(PROTOCOL_TLSv1_2))
    opener = request.build_opener(https_tls_handler)
    opener.addheaders = [('Accept-encoding', 'gzip')]
    request.install_opener(opener)
    if DEBUG:
        print(URL)
    try:
        fetch = opener.open(URL)
    except Exception as e:
        print('Failed to get ' + URL + ' for parsing. Error: ' + str(e))
        exit(1)
    return (fetch.info()['last-modified'])


def file_exists(filename):
    if DEBUG:
        print('enter: file_exists')
    try:
        open(filename)
        return True
    except IOError as e:
        print('Failed to locate ' + str(e))
    return


def load_timestamps_from_storage(filename):
    if DEBUG:
        print('enter: load_timestamps_from_storage')
    try:
        with open(filename, 'rb') as f:
            return load(f)
    except IOError as e:
        print('Failed to read ' + str(e))
    return


def fetch_timestamps():
    current_timestamps = {}
    if DEBUG:
        print('enter: fetch_timestamps')
    try:
        current_timestamps['cpu-last-modified'] = dt.strptime(
            last_modified(OpenWRT_CPU_Packages_Feeds_URL), '%a, %d %b %Y %H:%M:%S %Z')
        current_timestamps['target-last-modified'] = dt.strptime(
            last_modified(OpenWRT_Target_Release_URL + "/packages/Packages.gz"), '%a, %d %b %Y %H:%M:%S %Z')

        print("CPU feed last modified: " + str(current_timestamps['cpu-last-modified']))
        print("Target packages last modified: " + str(current_timestamps['target-last-modified']))
    except IOError as e:
        print('Failed to fetch ' + str(e))
    return current_timestamps


def save_timestamps(filename, obj):
    if DEBUG:
        print('enter: save_timestamps')
    try:
        with open(filename, 'wb') as f:
            dump(obj, f, HIGHEST_PROTOCOL)
    except IOError as e:
        print('Failed to write to ' + str(e))


def main():
    timestamps_from_storage = {}
    if file_exists(PICKLE_FILENAME):
        print('Reading previous timestamps')
        timestamps_from_storage = load_timestamps_from_storage(PICKLE_FILENAME)
        if DEBUG:
            print('Previous timestamps:')
            print(timestamps_from_storage)

    print('Fetching latest timestamps')
    timestamps_from_openwrt = fetch_timestamps()
    if DEBUG:
        print('Fetched timestamps')
        print(timestamps_from_openwrt)

    comparable = timestamps_from_openwrt == timestamps_from_storage
    if comparable:
        print('Comparable, not building.')
    else:
        print('Not comparable.')
        if file_exists(PICKLE_FILENAME):
            print("Stored CPU last modified: " + str(timestamps_from_storage['cpu-last-modified']))
            print("Stored target last modified: " + str(timestamps_from_storage['target-last-modified']))
        if DEBUG:
            print('Saving timestamps')
        save_timestamps(PICKLE_FILENAME, timestamps_from_openwrt)
        args = split(BUILDER_EXECUTABLE)
        print("Testing for " + args[0])
        if file_exists(args[0]):
            print("Building: " + str(args))
            rc = check_call(args)
        else:
            print('Unable to locate ' + BUILDER_EXECUTABLE)
            exit(1)
    # Stopping here
    # TODO: Complex parsing of feeds to only build when packages we need have been updated
    exit()

    feeds_soup = BeautifulSoup(cpu_feed.read().decode('utf-8'), "lxml")
    OpenWRT_Feed_Types = []

    for line in feeds_soup.find("p").text.splitlines():
        OpenWRT_Feed_Types.append(line.split(" ")[1])
        if DEBUG:
            print('Found ' + line.split(" ")[1])
    if DEBUG:
        print('Feed URLs')
        for feed in OpenWRT_Feed_Types:
            print(OpenWRT_CPU_Packages_URL + "/" + feed)

    for feed in OpenWRT_Feed_Types:
        url = OpenWRT_CPU_Packages_URL + "/" + feed + "/Packages.manifest"
        print('Fetching ' + feed + ' package names and SHA256sums')
        if DEBUG:
            print(OpenWRT_CPU_Packages_URL + "/" + feed + "/Packages.manifest")
        try:
            manifest = opener.open(url)
        except Exception as e:
            print('Failed to get ' + url + ' for parsing. Error: ' + str(e))
            exit(1)
        manifest_last_modified = manifest.info()['last-modified']
        print("Feed " + feed + " last modified " + manifest_last_modified)


if __name__ == "__main__":
    OpenWRT_CPU = "mips_24kc"  # mips_24kc, aarch64_cortex-a72 etc
    OpenWRT_Target = "ath79"  # ar71xx, x86 etc
    OpenWRT_Profile = "tl-wr1043nd-v1"  # tl-wr1043nd-v1, archer-c7-v1 etc.
    OpenWRT_Packages = ""  # luci-app-adblock luci-app-privoxy etc
    OpenWRT_Files = ""  # etc/config/openssl etc/openvpn/my_vpn.vpn etc.
    OpenWRT_Type = "generic"  # tiny or generic
    OpenWRT_Variant = "vanilla"  # SELF FREEFORM NAME
    OpenWRT_Core_Packages = "coreutils kmod-zram screen wget zram-swap"
    OpenWRT_Desirable_Packages = "luci-ssl ca-certificates ca-bundle zlib libopenssl"
    OpenWRT_USBStorage_Packages = "blkid block-mount e2fsprogs kmod-fs-ext4 kmod-usb-ohci kmod-usb-storage kmod-usb-uhci kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport sfdisk"
    OpenWRT_ExtraFileSystems_Packages = "kmod-fs-cramfs kmod-fs-jfs kmod-fs-vfat dosfstools"
    OpenWRT_OpenVPN_Packages = "luci-app-openvpn openvpn-openssl openssl-util openvpn-easy-rsa"
    OpenWRT_Privacy_Packages = "luci-app-adblock luci-app-privoxy luci-app-dnscrypt-proxy luci-app-https-dns-proxy luci-app-polipo luci-app-nextdns"
    OpenWRT_IPS_Packages = ""
    OpenWRT_Fwknop_Packages = "luci-app-fwknopd"
    OpenWRT_Nifty_Packages = "bash ip-full luci-app-commands luci-app-mwan3 luci-app-statistics luci-app-wifischedule luci-app-wol rsync terminfo"
    OpenWRT_CaptivePortal_Packages = "coova-chilli nodogsplash2 wifidog-tls"
    OpenWRT_User_Packages = "sudo shadow-common shadow-groupadd shadow-useradd shadow-usermod"
    OpenWRT_USB_Tethernet = "kmod-usb-net kmod-usb-net-rndis kmod-usb-net-cdc-ether"
    OpenWRT_Coreutils_Simple_Packages = "coreutils"
    OpenWRT_Coreutils_Packages = "coreutils coreutils-base64 coreutils-cat coreutils-chcon coreutils-chgrp coreutils-chmod coreutils-chown coreutils-chroot coreutils-cksum coreutils-comm coreutils-cp coreutils-csplit coreutils-date coreutils-dd coreutils-dircolors coreutils-echo coreutils-expand coreutils-factor coreutils-false coreutils-fmt coreutils-fold coreutils-hostid coreutils-install coreutils-join coreutils-kill coreutils-link coreutils-ln coreutils-logname coreutils-ls coreutils-mkdir coreutils-mknod coreutils-mktemp coreutils-mv coreutils-nice coreutils-nl coreutils-nohup coreutils-nproc coreutils-od coreutils-paste coreutils-pathchk coreutils-pinky coreutils-pr coreutils-printenv coreutils-ptx coreutils-pwd coreutils-realpath coreutils-rm coreutils-rmdir coreutils-runcon coreutils-sha1sum coreutils-sha224sum coreutils-sha384sum coreutils-sha512sum coreutils-shred coreutils-shuf coreutils-sleep coreutils-split coreutils-stat coreutils-stdbuf coreutils-stty coreutils-sum coreutils-sync coreutils-tac coreutils-timeout coreutils-touch coreutils-true coreutils-truncate coreutils-tsort coreutils-tty coreutils-uname coreutils-unexpand coreutils-unlink coreutils-users coreutils-vdir coreutils-who coreutils-whoami"
    OpenWRT_Procpsng_Simple_Packages = "procps-ng"
    OpenWRT_Procpsng_Packages = "procps-ng procps-ng-free procps-ng-kill procps-ng-pgrep procps-ng-pkill procps-ng-pmap procps-ng-ps procps-ng-pwdx procps-ng-skill procps-ng-slabtop procps-ng-snice procps-ng-tload procps-ng-top procps-ng-uptime procps-ng-vmstat procps-ng-w procps-ng-watch"
    OpenWRT_User_Simple_Packages = "sudo shadow-common shadow-groupadd shadow-useradd shadow-usermod"
    OpenWRT_User_Packages = "sudo shadow-chage shadow-chpasswd shadow-chfn shadow-chsh shadow-expiry shadow-faillog shadow-gpasswd shadow-groupadd shadow-groupdel shadow-groupmems shadow-groupmod shadow-groups shadow-lastlog shadow-login shadow-newgidmap shadow-newgrp shadow-newuidmap shadow-nologin shadow-passwd shadow-su shadow-useradd shadow-userdel shadow-usermod shadow-vipw"
    OpenWRT_Shell = "bash"
    OpenWRT_Stats_Simple_Packages = "luci-app-statistics"
    OpenWRT_Stats_Packages = "luci-app-statistics collectd collectd-mod-conntrack collectd-mod-cpu collectd-mod-dns collectd-mod-entropy collectd-mod-interface collectd-mod-iptables collectd-mod-iwinfo collectd-mod-load collectd-mod-memory collectd-mod-netlink collectd-mod-network collectd-mod-openvpn collectd-mod-ping collectd-mod-processes collectd-mod-protocols collectd-mod-rrdtool collectd-mod-tcpconns collectd-mod-wireless"
    OpenWRT_Image_Builder = "OpenWRT-imagebuilder-" + OpenWRT_Release_Version + "-" + OpenWRT_Target + "-" + OpenWRT_Type + ".Linux-x86_64"
    OpenWRT_Image_Builder_Archive = OpenWRT_Image_Builder + ".tar.xz"
    OpenWRT_Target_Release_URL = HTTP + "://downloads.OpenWRT.org/releases/" + OpenWRT_Release_Version + "/targets/" + OpenWRT_Target + "/" + OpenWRT_Type
    OpenWRT_Target_Packages_URL = OpenWRT_Target_Release_URL + "/" + "packages"
    OpenWRT_Target_Builder_URL = OpenWRT_Target_Release_URL + "/" + OpenWRT_Image_Builder_Archive
    OpenWRT_Target_sha256sums = OpenWRT_Target_Release_URL + "/sha256sums"
    OpenWRT_CPU_Packages_URL = HTTP + "://downloads.OpenWRT.org/releases/" + OpenWRT_Release_Version + "/packages/" + OpenWRT_CPU
    OpenWRT_CPU_Packages_Feeds_URL = OpenWRT_CPU_Packages_URL + "/feeds.conf"
    if DEBUG:
        print("Started : " + str(RUNTIME))
    main()
    if DEBUG:
        print("Finished: " + str(dt.now()))
        print("Elapsed : " + str(dt.now() - RUNTIME))
