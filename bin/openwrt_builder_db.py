#!/usr/bin/env python3
"""Module docstring.

A really long usage message should go here
"""
from openwrt_builder_globals import DEBUG, HTTP, RUNTIME, SQLALCHEMY_FILENAME


from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy import event
from sqlalchemy.orm import sessionmaker

Engine = create_engine('sqlite:///' + SQLALCHEMY_FILENAME, echo=DEBUG)
DBSession = sessionmaker(bind=Engine)
session = DBSession()


@event.listens_for(Engine, "connect")
def do_connect(dbapi_connection):
    # disable pysqlite's emitting of the BEGIN statement entirely.
    # also stops it from emitting COMMIT before any DDL.
    dbapi_connection.isolation_level = None


@event.listens_for(Engine, "begin")
def do_begin(conn):
    # emit our own BEGIN
    conn.execute("BEGIN")



class Packages(session):
    # Tell SQLAlchemy what the table name is and if there's any table-specific arguments it should know about
    __tablename__ = 'packages'
    __table_args__ = {'sqlite_autoincrement': True}
    # tell SQLAlchemy the name of column and its attributes:
    id = Column(Integer, primary_key=True, nullable=False)
    source = Column(String)  # Feed
    name = Column(String)  # Packages
    depends = Column(String)  # Depends
    version = Column(String)  # Version
    size = Column(Integer)  # Size
    filename = Column(String)  # filename
    sha256sum = Column(String)  # SHA256Sum

