#!/bin/bash
#
# make-openwrt.sh - build openwrt stuff
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#################################################################################

set -e
set -o pipefail
set -u
IFS=$'\n\t'

if [ ! $UID ]; then
  /bin/echo "No UID set, assuming we're running in a CI or similar"
  UID=0
else
  error_report() {
    /bin/echo "Error on line $1"
  }
  trap 'error_report $LINENO' ERR
fi

init() {
  BUILD_TYPE=""
  DEB_PKGS=""
  HELP=""
  ftp_proxy=""
  FTP_PROXY=""
  http_proxy=""
  HTTP_PROXY=""
  https_proxy=""
  HTTPS_PROXY=""
  HELP=false
  DEB_PKGS=""
  HAVE_SUDO=""
  NICE="/usr/bin/ionice --class 3 nice --adjustment=19"
  WGET="/usr/bin/wget"

  BUILD_DIR=$HOME/Downloads/openwrt-build
  OPENWRT_RELEASE_VERSION=19.07.5
  BUILD_STATUSFILE=$PWD/build.log
  if [ -f $BUILD_STATUSFILE ]; then rm $BUILD_STATUSFILE; fi
  CONFIG_TARGET_BOARD=(ath79)
  PLATFORMS_GENERIC=(archer-c7-v2 tl-wdr4300-v1 tl-wr1043nd-v1)
  PLATFORMS_TINY=(tl-wa850re-v1)

  SPARE_CORES=2
  JOBS="--jobs=$(($(/bin/grep proc /proc/cpuinfo | /usr/bin/wc --lines) - $SPARE_CORES))"
  VERBOSE=false
  IGNORE_ERRORS="IGNORE_ERRORS=s"
  VERBOSE_BUILD="V=0"
  BUILD_LOG="BUILD_LOG=1"
  HTTP="https"
  MAKE_HTTP_PROXY=""
  IMAGEBUILD_DIR_SHA256SUM=""
  APT_QUIET="--quiet --quiet"
  WGET_VERBOSE="--no-verbose"
  VERBOSE_SWITCH=""

  OPENWRT_PACKAGES_FWKNOP="luci-app-fwknopd"

  OPENWRT_PACKAGES_BASE="screen wget ca-certificates ca-bundle"
  OPENWRT_PACKAGES_DESIRABLE="coreutils-cat gawk grep kmod-zram libopenssl terminfo zram-swap"
  OPENWRT_PACKAGES_LUCISSL="luci-ssl"
  OPENWRT_PACKAGES_USBSTORAGE="blkid block-mount e2fsprogs f2fsck f2fs-tools kmod-fs-ext4 kmod-fs-f2fs kmod-usb-ohci kmod-usb-storage kmod-usb-uhci kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport mkf2fs sfdisk usbreset usbutils"
  OPENWRT_PACKAGES_EXTRAFILESYSTEM="kmod-fs-autofs4 kmod-fs-cramfs kmod-fs-isofs kmod-fs-jfs kmod-loop kmod-fs-nfs-v3 kmod-fs-nfs-v4 kmod-fs-vfat dosfstools"
  OPENWRT_PACKAGES_OPENVPN="luci-app-openvpn openvpn-openssl openssl-util liblz4 liblzo gnupg gnupg-utils"
  OPENWRT_PACKAGES_DNSALTERNATES="luci-app-dnscrypt-proxy luci-app-https-dns-proxy luci-app-nextdns"
  OPENWRT_PACKAGES_PRIVACY="luci-app-adblock luci-app-privoxy luci-app-polipo "

  OPENWRT_PACKAGES_IPS=" kismet-drone snort"
  OPENWRT_PACKAGES_TFTP="atftp atftpd"
  OPENWRT_PACKAGES_NIFTY="bash ip-full luci-app-commands luci-app-mwan3 luci-app-statistics luci-app-wifischedule luci-app-wol rsync terminfo"
  OPENWRT_PACKAGES_USBTETHERNET="kmod-usb-net kmod-usb-net-rndis kmod-usb-net-cdc-ether usbreset usbutils"
  OPENWRT_PACKAGES_TOR="tor tor-gencert tor-geoip tor-resolve"
  OPENWRT_PACKAGES_DNSMASQFULL="-dnsmasq dnsmasq-full"
  OPENWRT_PACKAGES_SHELL="bash"

  # Groups
  # NFS Client
  ##    OPENWRT_PACKAGES_NFSCLIENT_SIMPLE="kmod-fs-nfs-v4"
  ##    OPENWRT_PACKAGES_NFSCLIENT="kmod-fs-nfs-v4 kmod-fs-nfs-v3 nfs-utils kmod-fs-nfs-common-rpcsec kmod-fs-autofs4"
  OPENWRT_PACKAGES_NFSCLIENT=""
  # NFS Client
  ##    OPENWRT_PACKAGES_NFSSERVER_SIMPLE="nfs-kernel-server"
  ##    OPENWRT_PACKAGES_NFSSERVER="kmod-fs-nfsd kmod-fs-nfs portmap nfs-utils nfs-kernel-server-utils kmod-fs-nfs-common-rpcsec"
  OPENWRT_PACKAGES_NFSSERVER=""
  # Captive Portal
  ##    OPENWRT_PACKAGES_CAPTIVEPORTAL_SIMPLE="nodogsplash2"
  OPENWRT_PACKAGES_CAPTIVEPORTAL_SIMPLE=""
  OPENWRT_PACKAGES_CAPTIVEPORTAL="coova-chilli wifidog-tls"
  # Core Utilities
  OPENWRT_PACKAGES_COREUTILS_SIMPLE="coreutils"
  OPENWRT_PACKAGES_COREUTILS="coreutils coreutils-base64 coreutils-cat coreutils-chcon coreutils-chgrp coreutils-chmod coreutils-chown coreutils-chroot coreutils-cksum coreutils-comm coreutils-cp coreutils-csplit coreutils-date coreutils-dd coreutils-dircolors coreutils-echo coreutils-expand coreutils-factor coreutils-false coreutils-fmt coreutils-fold coreutils-hostid coreutils-install coreutils-join coreutils-kill coreutils-link coreutils-ln coreutils-logname coreutils-ls coreutils-mkdir coreutils-mknod coreutils-mktemp coreutils-mv coreutils-nice coreutils-nl coreutils-nohup coreutils-nproc coreutils-od coreutils-paste coreutils-pathchk coreutils-pinky coreutils-pr coreutils-printenv coreutils-ptx coreutils-pwd coreutils-realpath coreutils-rm coreutils-rmdir coreutils-runcon coreutils-sha1sum coreutils-sha224sum coreutils-sha384sum coreutils-sha512sum coreutils-shred coreutils-shuf coreutils-sleep coreutils-split coreutils-stat coreutils-stdbuf coreutils-stty coreutils-sum coreutils-sync coreutils-tac coreutils-timeout coreutils-touch coreutils-true coreutils-truncate coreutils-tsort coreutils-tty coreutils-uname coreutils-unexpand coreutils-unlink coreutils-users coreutils-vdir coreutils-who coreutils-whoami"
  # Procps-NG
  OPENWRT_PACKAGES_PROCPSNG_SIMPLE="procps-ng"
  OPENWRT_PACKAGES_PROCPSNG="procps-ng procps-ng-free procps-ng-kill procps-ng-pgrep procps-ng-pkill procps-ng-pmap procps-ng-ps procps-ng-pwdx procps-ng-skill procps-ng-slabtop procps-ng-snice procps-ng-tload procps-ng-top procps-ng-uptime procps-ng-vmstat procps-ng-w procps-ng-watch"
  # Users
  OPENWRT_PACKAGES_USER_SIMPLE="sudo shadow-common shadow-groupadd shadow-useradd shadow-usermod"
  OPENWRT_PACKAGES_USER="sudo shadow-chage shadow-chpasswd shadow-chfn shadow-chsh shadow-expiry shadow-faillog shadow-gpasswd shadow-groupadd shadow-groupdel shadow-groupmems shadow-groupmod shadow-groups shadow-lastlog shadow-login shadow-newgidmap shadow-newgrp shadow-newuidmap shadow-nologin shadow-passwd shadow-su shadow-useradd shadow-userdel shadow-usermod shadow-vipw"
  # Statistics
  OPENWRT_PACKAGES_STATS_SIMPLE="luci-app-statistics"
  OPENWRT_PACKAGES_STATS="luci-app-statistics collectd collectd-mod-conntrack collectd-mod-cpu collectd-mod-dns collectd-mod-entropy collectd-mod-interface collectd-mod-iptables collectd-mod-iwinfo collectd-mod-load collectd-mod-memory collectd-mod-netlink collectd-mod-network collectd-mod-openvpn collectd-mod-ping collectd-mod-processes collectd-mod-protocols collectd-mod-rrdtool collectd-mod-tcpconns collectd-mod-wireless"

  # Modifications for home network services
  OPENWRT_NOPACKAGES_HOMENETWORK="-ppp -ppp-mod-pppoe"
  OPENWRT_NOPACKAGES_WIFI="-kmod-cfg80211 -wireless-regdb -kmod-cfg80211 -hostapd-common -kmod-mac80211 -kmod-ath9k -kmod-ath10k -kmod-ath9k-common -ath10k-firmware-qca988x -wpad-mini -libiwinfo-lua -libiwinfo -collectd-mod-iwinfo -iw -wpad-mini -luci-app-wifischedule"
  OPENWRT_PACKAGES_THEME="-luci-theme-bootstrap -luci-theme-openwrt luci-theme-material"
}

chk_dpkg() {
  LC_ALL=C /usr/bin/dpkg --list | /bin/grep ^ii | /usr/bin/awk '{print $2}' | /bin/grep "^${pkg}" >/dev/null || DEB_PKGS="${DEB_PKGS}${pkg} "
}

debian_packages_check() {
  /bin/echo "Checking for pre-requisite Debian packages"
  DEBIAN_PACKAGES=$*
  for pkg in $DEBIAN_PACKAGES; do
    chk_dpkg
  done

  if [ "$DEB_PKGS" ]; then
    /bin/echo "Need packages: $DEB_PKGS"
    if [ "$UID" != "0" ]; then
      /bin/echo "Not root user"
      # NOTE: Do not explictly add any path to this test
      #       We're effectivly searching in $PATH
      if hash sudo 2>/dev/null; then
        HAVE_SUDO=$(command -v sudo 2>/dev/null)
        /bin/echo "sudo(1) found at $HAVE_SUDO"
      else
        /bin/echo "Need root/sudo access"
        exit 1
      fi
    fi
    /bin/echo "Updating Debian package cache"
    $HAVE_SUDO bash -c "DEBIAN_FRONTEND=noninteractive apt $APT_QUIET update"
    /bin/echo "Checking and installing needed Debian packages"
    $HAVE_SUDO bash -c "DEBIAN_FRONTEND=noninteractive apt --assume-yes $APT_QUIET install $DEB_PKGS"
  else
    /bin/echo "Debian package check passed"
  fi
}

debian_backports_packages_check() {
  /bin/echo "Checking for Debian pre-requisite backport packages"
  DEBIAN_PACKAGES=$*
  for pkg in $DEBIAN_PACKAGES; do
    /bin/echo "Testing $pkg"
    chk_dpkg
  done

  if [ "$DEB_PKGS" ]; then
    /bin/echo "Need Debian packages: $DEB_PKGS"
    if [ "$UID" != "0" ]; then
      /bin/echo "Not root user"
      # NOTE: Do not explictly add any path to this test
      #       We're effectivly searching in $PATH
      if hash sudo 2>/dev/null; then
        HAVE_SUDO=$(command -v sudo 2>/dev/null)
        /bin/echo "sudo(1) found at $HAVE_SUDO"
      else
        /bin/echo "Need root/sudo access"
        exit 1
      fi
    fi
    /bin/echo "Updating Debian package cache"
    $HAVE_SUDO bash -c "DEBIAN_FRONTEND=noninteractive apt -t stretch-backports $APT_QUIET update"
    /bin/echo "Checking and installing needed Debian packages"
    $HAVE_SUDO bash -c "DEBIAN_FRONTEND=noninteractive apt --assume-yes -t stretch-backports $APT_QUIET install $DEB_PKGS"
  else
    /bin/echo "Debian package check passed"
  fi
}

imagebuild_debian_check() {
  DEBIAN_PACKAGES=(build-essential ccache file g++ gawk gettext git libncurses5-dev libssl-dev man-db python2.7 rsync subversion time unzip wget xsltproc zip zlib1g-dev)
  debian_packages_check ${DEBIAN_PACKAGES[*]}
}

fullbuild_debian_check() {
  PERL_MODULES="perl-modules-5.24"
  JAVA="openjdk-8-jdk"
  if [ ! "$(grep -E 'buster|sid' /etc/os-release)" = "" ]; then
    PERL_MODULES="perl-modules-5.26"
    debian_backports_packages_check openjdk-8-jdk
    JAVA=""
  fi
  # https://openwrt.org/docs/guide-developer/build-system/install-buildsystem#prerequisites
  DEBIAN_PACKAGES_NEEDED=(asciidoc bash bc binutils bzip2 fastjar flex g++ gawk genisoimage gettext gettext git intltool jikespg libgtk2.0-dev libncurses5-dev libssl-dev make mercurial patch "$PERL_MODULES" python2.7-dev quilt rsync ruby sdcc unzip util-linux wget xsltproc zlib1g-dev)
  PARTIAL_REQUIREMENTS=(bcc bin86 build-essential gawk gettext git libboost-dev libncurses5-dev libssl-dev libusb-dev libxml-parser-perl $JAVA sharutils subversion swig time unzip zlib1g-dev)
  DEBIAN_PACKAGES=("${DEBIAN_PACKAGES_NEEDED[@]}" "${PARTIAL_REQUIREMENTS[@]}")
  debian_packages_check ${DEBIAN_PACKAGES[*]}
}

imagebuild_prep() {
  if [ $# -ne 2 ]; then
    /bin/echo "Incorrect number of arguments passed to builder()"
    exit 1
  fi
  local TARGET=$1
  local TYPE=$2
  /bin/echo "Prepping for $TARGET $TYPE"
  if [ $OPENWRT_RELEASE_VERSION != "snapshots" ]; then
    OPENWRT_IMAGEBUILDER=openwrt-imagebuilder-$OPENWRT_RELEASE_VERSION-$TARGET-$TYPE.Linux-x86_64
    OPENWRT_IMAGEBUILDER_URL=$HTTP://downloads.openwrt.org/releases/$OPENWRT_RELEASE_VERSION/targets/$TARGET/$TYPE/
  else
    OPENWRT_IMAGEBUILDER=openwrt-imagebuilder-$TARGET-$TYPE.Linux-x86_64
    OPENWRT_IMAGEBUILDER_URL=$HTTP://downloads.openwrt.org/snapshots/targets/$TARGET/$TYPE/
  fi
  OPENWRT_IMAGEBUILDER_ARCHIVE=$OPENWRT_IMAGEBUILDER.tar.xz
  OPENWRT_SHA256SUMS_URL=$OPENWRT_IMAGEBUILDER_URL/sha256sums
  if [[ -f openwrt.$TYPE.sha256sums ]]; then
    /bin/rm openwrt.$TYPE.sha256sums
  fi
  $WGET $WGET_VERBOSE --output-document=openwrt.$TYPE.sha256sums $OPENWRT_SHA256SUMS_URL
  if [ ! -f $OPENWRT_IMAGEBUILDER_ARCHIVE ]; then
    /bin/echo "Downloading $OPENWRT_IMAGEBUILDER_ARCHIVE"
    $WGET $WGET_VERBOSE $OPENWRT_IMAGEBUILDER_URL/$OPENWRT_IMAGEBUILDER_ARCHIVE
  else
    /bin/echo "Found an existing builder for $OPENWRT_IMAGEBUILDER_ARCHIVE"
  fi
  OPENWRT_IMAGEBUILDER_FILE_256SUM_ACTUAL="$(/usr/bin/sha256sum $OPENWRT_IMAGEBUILDER_ARCHIVE | /usr/bin/awk '{print $1}')"
  OPENWRT_IMAGEBUILDER_FILE_256SUM_EXPECTED="$(/bin/grep $OPENWRT_IMAGEBUILDER_ARCHIVE openwrt.$TYPE.sha256sums | /usr/bin/awk '{print $1}')"
  /bin/echo "Image builder actual sha256sum   : $OPENWRT_IMAGEBUILDER_FILE_256SUM_ACTUAL"
  /bin/echo "Image builder expected sha256sum : $OPENWRT_IMAGEBUILDER_FILE_256SUM_EXPECTED"
  if [ "$OPENWRT_IMAGEBUILDER_FILE_256SUM_ACTUAL" != "$OPENWRT_IMAGEBUILDER_FILE_256SUM_EXPECTED" ]; then
    /bin/echo "sha256sum mismatch"
    exit 1
  fi

  if [ -d $OPENWRT_IMAGEBUILDER ]; then
    /bin/echo "Clearing out existing $OPENWRT_IMAGEBUILDER directory"
    /bin/rm --force --recursive $OPENWRT_IMAGEBUILDER
  fi
  /bin/echo "Extracting $OPENWRT_IMAGEBUILDER_ARCHIVE"
  /bin/tar --extract $VERBOSE_SWITCH --file $OPENWRT_IMAGEBUILDER_ARCHIVE
  if [ $OPENWRT_RELEASE_VERSION != "snapshots" ]; then
    /bin/echo "Patching makefile for a better clean /usr/bin/patch  -p0 < patches/$OPENWRT_IMAGEBUILDER.Makefile.patch"
    /usr/bin/patch --strip=0 <patches/$OPENWRT_IMAGEBUILDER.Makefile.patch
  fi

  if [ -z "$HTTP_PROXY" ]; then
    /bin/sed -i s/http/https/g $OPENWRT_IMAGEBUILDER/repositories.conf
  else
    export MAKE_HTTP_PROXY
  fi

}

fullbuild_pre() {

  if [ ! -d ${BUILD_DIR} ]; then
    /bin/echo "Making ${BUILD_DIR}"
    mkdir --parents ${BUILD_DIR}
  fi

  if [ ! -d ${BUILD_DIR}/src ]; then
    /bin/echo "git clone git://git.openwrt.org/openwrt/openwrt.git ${BUILD_DIR}/src"
    /usr/bin/git clone git://git.openwrt.org/openwrt/openwrt.git ${BUILD_DIR}/src
  else
    /bin/echo "Attempting to update ${BUILD_DIR}/src"
    /bin/echo "This may fail as it isn't documented upstream"
    /bin/echo "git -C ${BUILD_DIR}/src checkout origin/master"
    /usr/bin/git -C ${BUILD_DIR}/src checkout origin/master
    /bin/echo "git -C ${BUILD_DIR}/src fetch --all"
    /usr/bin/git -C ${BUILD_DIR}/src fetch --all
  fi

  /bin/echo "Checking out version $OPENWRT_RELEASE_VERSION"
  /usr/bin/git -C ${BUILD_DIR}/src checkout "v$OPENWRT_RELEASE_VERSION"

  /bin/echo "Updating feeds"
  ${BUILD_DIR}/src/scripts/feeds update -a

  /bin/echo "Installing feeds"
  ${BUILD_DIR}/src/scripts/feeds install -a

  # https://github.com/openwrt/telephony/issues/364
  # possibly fixed in https://github.com/openwrt/packages/commit/890993d4063c31ff04ef3e5fa1a51444e4d504c9
  # /bin/echo "Working around bug in telephony feed"
  # ${BUILD_DIR}/src/scripts/feeds update -a
  # ${BUILD_DIR}/src/scripts/feeds install -a

}

fullbuild_customise_config() {
  # TODO: Read in and enforce base required packages
  # TODO: Differentialte tiny and generic
  /bin/echo "Removing old config"
  /bin/rm --force ${BUILD_DIR}/src/.config

  /bin/echo "Writing our bare bones custom ${BUILD_DIR}/src/.config"
  /bin/cat >${BUILD_DIR}/src/.config <<EOF
CONFIG_TARGET_ath79=y
CONFIG_TARGET_ath79_generic=y
CONFIG_TARGET_MULTI_PROFILE=y
CONFIG_TARGET_DEVICE_ath79_generic_DEVICE_archer-c7-v2=y
CONFIG_TARGET_DEVICE_ath79_generic_DEVICE_tl-wdr4300-v1=y
CONFIG_TARGET_DEVICE_ath79_generic_DEVICE_tl-wr1043nd-v1=y
# CONFIG_DRIVER_11AC_SUPPORT is not set
# CONFIG_PACKAGE_ath10k-firmware-qca988x is not set
# CONFIG_PACKAGE_kmod-ath10k is not set
CONFIG_PACKAGE_kmod-usb-ohci=y
EOF

  /bin/echo "Retrieving upstream non-CONFIG_TARGET config info for ${BUILD_DIR}/src/.config"
  $WGET --no-verbose -O - https://downloads.openwrt.org/releases/$OPENWRT_RELEASE_VERSION/targets/ath79/generic/config.seed |
    /bin/grep -v CONFIG_TARGET_ >>${BUILD_DIR}/src/.config
  /bin/echo "Retrieving upstream manifest and adding ot to ${BUILD_DIR}/src/.config"
  $WGET --no-verbose -O - https://downloads.openwrt.org/releases/$OPENWRT_RELEASE_VERSION/targets/ath79/generic/openwrt-$OPENWRT_RELEASE_VERSION-ath79-generic.manifest |
    /usr/bin/awk '{print $1}' | /bin/sed s/^/CONFIG_PACKAGE_/g | /bin/sed s/$/=y/g >>${BUILD_DIR}/src/.config

  /bin/echo "Making a basic ${BUILD_DIR}/src/.config based upon above selection"
  /bin/echo "make -C ${BUILD_DIR}/src $BUILD_LOG $JOBS $IGNORE_ERRORS $VERBOSE_BUILD defconfig"
  /usr/bin/make -C ${BUILD_DIR}/src $BUILD_LOG $JOBS $IGNORE_ERRORS $VERBOSE_BUILD defconfig

  /bin/echo "Modularising everything that's not already set in ${BUILD_DIR}/src/.config"
  /bin/sed --in-place s/^#\ CONFIG_PACKAGE_/CONFIG_PACKAGE_/g ${BUILD_DIR}/src/.config
  /bin/sed --in-place '/^CONFIG_PACKAGE_.* is not set$/ s/ is not set$/=m/g' ${BUILD_DIR}/src/.config

  /bin/echo "Finalising ${BUILD_DIR}/src/.config"
  /usr/bin/make -C ${BUILD_DIR}/src $BUILD_LOG $JOBS $IGNORE_ERRORS $VERBOSE_BUILD defconfig 2>/dev/null

  /bin/echo "Downloading selected build requirements"
  /bin/echo "make -C ${BUILD_DIR}/src $BUILD_LOG $JOBS $IGNORE_ERRORS $VERBOSE_BUILD download"
  /usr/bin/make -C ${BUILD_DIR}/src $BUILD_LOG $JOBS $IGNORE_ERRORS $VERBOSE_BUILD download

}

imagebuilder() {
  #   TARGET PROFILE TYPE NICKNAME PACKAGES FILES
  local TARGET=$1
  local PROFILE=$2
  local TYPE=$3
  local NICKNAME=$4
  local PACKAGES=$5
  local FILES=$6
  /bin/echo "Building $TARGET $PROFILE $TYPE $NICKNAME"

  CWD=$(/bin/pwd)

  # Copy in $FILES before buildind
  if [ -d $FILES ] && [ "$FILES" != "" ]; then
    /bin/cp --preserve --recursive $VERBOSE_SWITCH $FILES $CWD/$OPENWRT_IMAGEBUILDER/.
  fi

  cd $OPENWRT_IMAGEBUILDER

  # Build
  /usr/bin/make image \
    PROFILE="$PROFILE" \
    PACKAGES="$PACKAGES" \
    FILES="$FILES" \
    EXTRA_IMAGE_NAME="$NICKNAME" \
    BIN_DIR="$CWD/$TARGET/$TYPE/$NICKNAME/."

  # TODO: Write a failed file if building an image fails
  #    if [ $? = 0 ]; then
  #        echo "$TARGET $NICKNAME $TYPE : SUCCESS " >> $BUILD_STATUSFILE
  #    else
  #        echo "$TARGET $NICKNAME $TYPE : FAIL" >> $BUILD_STATUSFILE
  #    fi

  # Ensure we're back where we were earlier
  cd $CWD

}

hammer() {
  # all the things
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "hammer" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_EXTRAFILESYSTEM $OPENWRT_PACKAGES_OPENVPN $OPENWRT_PACKAGES_FWKNOP $OPENWRT_PACKAGES_PRIVACY $OPENWRT_PACKAGES_IPS $OPENWRT_PACKAGES_NIFTY $OPENWRT_PACKAGES_STATS $OPENWRT_PACKAGES_COREUTILS $OPENWRT_PACKAGES_PROCPSNG $OPENWRT_PACKAGES_USER $OPENWRT_PACKAGES_SHELL $OPENWRT_PACKAGES_USBTETHERNET $OPENWRT_PACKAGES_TFTP $OPENWRT_PACKAGES_DNSMASQFULL luci-app-ddns luci-app-nlbwmon luci-app-qos luci-app-sqm luci-app-commands luci-app-cjdns luci-app-acme $OPENWRT_PACKAGES_THEME terminfo vim" "files/"
}

ispconnect() {
  # boxes to connect to an upstream ISP
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "ispconnect-full" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_OPENVPN $OPENWRT_PACKAGES_FWKNOP $OPENWRT_PACKAGES_PRIVACY $OPENWRT_PACKAGES_IPS $OPENWRT_PACKAGES_NIFTY $OPENWRT_PACKAGES_SHELL $OPENWRT_PACKAGES_COREUTILS $OPENWRT_PACKAGES_PROCPSNG $OPENWRT_PACKAGES_USER $OPENWRT_PACKAGES_USBTETHERNET $OPENWRT_PACKAGES_DNSMASQFULL luci-app-ddns luci-app-nlbwmon luci-app-qos luci-app-sqm luci-app-commands luci-app-cjdns luci-app-acme $OPENWRT_PACKAGES_THEME terminfo vim" "files/"
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "ispconnect-full-nowifi" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_OPENVPN $OPENWRT_PACKAGES_FWKNOP $OPENWRT_PACKAGES_PRIVACY $OPENWRT_PACKAGES_IPS $OPENWRT_PACKAGES_NIFTY $OPENWRT_PACKAGES_SHELL $OPENWRT_PACKAGES_COREUTILS $OPENWRT_PACKAGES_PROCPSNG $OPENWRT_PACKAGES_USER $OPENWRT_PACKAGES_USBTETHERNET $OPENWRT_PACKAGES_DNSMASQFULL luci-app-ddns luci-app-nlbwmon luci-app-qos luci-app-sqm luci-app-commands luci-app-cjdns luci-app-acme $OPENWRT_PACKAGES_THEME terminfo vim $OPENWRT_NOPACKAGES_WIFI" "files/"
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "ispconnect-usbstorage-openvpn" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_OPENVPN $OPENWRT_PACKAGES_SHELL luci-app-nlbwmon $OPENWRT_PACKAGES_THEME" "files/"
}

homenetwork() {
  # routers, switches, vpns etc.
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "homenetwork-fulldnsmasq-nfsserver-tftpd-nowifi" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_DNSMASQFULL $OPENWRT_PACKAGES_NFSCLIENT $OPENWRT_PACKAGES_TFTP kmod-fs-isofs kmod-loop luci-app-wol $OPENWRT_PACKAGES_THEME $OPENWRT_NOPACKAGES_HOMENETWORK $OPENWRT_NOPACKAGES_WIFI" "files/"
  imagebuilder "ath79" "tplink_tl-wdr4300-v1" "generic" "homenetwork-fulldnsmasq-nfsserver-tftpd-nowifi" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_DNSMASQFULL $OPENWRT_PACKAGES_NFSCLIENT $OPENWRT_PACKAGES_TFTP kmod-fs-isofs kmod-loop luci-app-wol $OPENWRT_PACKAGES_THEME $OPENWRT_NOPACKAGES_HOMENETWORK $OPENWRT_NOPACKAGES_WIFI" "files/"
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "homenetwork-usbstorage-openvpn" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_OPENVPN $OPENWRT_PACKAGES_SHELL luci-app-nlbwmon $OPENWRT_PACKAGES_THEME $OPENWRT_NOPACKAGES_HOMENETWORK" "files/"
  imagebuilder "ath79" "tplink_tl-wr1043nd-v1" "generic" "homenetwork-usbstorage-nowifi" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_THEME $OPENWRT_NOPACKAGES_HOMENETWORK $OPENWRT_NOPACKAGES_WIFI" "files/"
}

usbstorage() {
  # simple box with a usb port
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "usbstorage" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_THEME" "files/"
  imagebuilder "ath79" "tplink_tl-wdr4300-v1" "generic" "usbstorage" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_DESIRABLE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_THEME" "files/"
  imagebuilder "ath79" "tplink_tl-wr1043nd-v1" "generic" "usbstorage-mini" "$OPENWRT_PACKAGES_BASE $OPENWRT_PACKAGES_LUCISSL $OPENWRT_PACKAGES_USBSTORAGE $OPENWRT_PACKAGES_THEME" "files/"
}

vanilla() {
  imagebuilder "ath79" "tplink_archer-c7-v2" "generic" "vanilla" "" ""
  imagebuilder "ath79" "tplink_tl-wdr4300-v1" "generic" "vanilla" "" ""
  imagebuilder "ath79" "tplink_tl-wr1043nd-v1" "generic" "vanilla" "" ""
}

tiny() {
  #   TARGET PROFILE TYPE NICKNAME PACKAGES FILES
  echo "tl-wa850re does not have enough memory to run. skipping"
  # imagebuilder "ath79" "tplink_tl-wa850re-v1" "tiny" "homenetwork-luci" "$OPENWRT_NOPACKAGES_HOMENETWORK luci" ""
  # imagebuilder "ath79" "tplink_tl-wa850re-v1" "tiny" "vanilla" "" ""
}

imagebuild_routines() {
  # Prep build area here so it's not duplicated
  imagebuild_prep "ath79" "generic"
  hammer
  ispconnect
  homenetwork
  usbstorage
  vanilla
  # Prep build area here so it's not duplicated
  #    imagebuild_prep "ath79" "tiny"
  #    tiny
}

fullbuild() {
  /bin/echo "Now building ${BUILD_DIR}/src/.config"
  /bin/echo "make -C ${BUILD_DIR}/src $BUILD_LOG $JOBS $IGNORE_ERRORS $VERBOSE"
  /usr/bin/make -C ${BUILD_DIR}/src $BUILD_LOG $JOBS $IGNORE_ERRORS $VERBOSE
}

imagebuild_cleanup() {
  CWD=$(/bin/pwd)
  cd $OPENWRT_IMAGEBUILDER
  /usr/bin/make clean
  cd $CWD
  /bin/rm --force $VERBOSE_SWITCH *sha256sums
}

usage() {
  /bin/cat <<EOF
Usage: $0 -h|-p [PROXY]|-t (full,images)|-v
-h        : this help
-p PROXY  : proxy address
-t TYPE   : full | images
-v        : verbose
EOF
  exit 0
}

init
# TODO: Fix multiple options
if ! OPTS=$(getopt --options hp:t:v --longoptions help,proxy:,type:,verbose -n 'parse-options' -- "$@"); then
  /bin/echo "Failed parsing options." >&2
  usage
  exit 1
fi
eval set --$OPTS
while [[ $# -gt 0 ]]; do
  case "$1" in
  -h | --help)
    HELP=true
    shift
    ;;
  -p | --proxy)
    /bin/echo "Proxy set: $2"
    export ftp_proxy=$2
    export FTP_PROXY=$2
    export http_proxy=$2
    export HTTP_PROXY=$2
    export https_proxy=$2
    export HTTPS_PROXY=$2
    MAKE_HTTP_PROXY="http_proxy=$HTTP_PROXY"
    HTTP="http"
    shift 2
    ;;
  -t | --type)
    BUILDTYPE=$2
    /bin/echo "Type set: $2"
    if ! [ "$BUILDTYPE" = "images" ] && ! [ "$BUILDTYPE" = "full" ]; then
      /bin/echo "Bad build type ($BUILDTYPE) specified!"
      usage
      exit 1
    fi
    shift 2
    ;;
  -v | --verbose)
    /bin/echo "Verbose set"
    VERBOSE=true
    VERBOSE_SWITCH="--verbose"
    APT_QUIET=""
    VERBOSE_BUILD="V=99"
    WGET_VERBOSE="--verbose"
    shift
    ;;
  --)
    shift
    break
    ;;
  *)
    break
    ;;
  esac
done
if [ "$HELP" = "true" ] || [ "$BUILDTYPE" = "" ]; then
  usage
  exit 0
fi
if [ "$BUILDTYPE" = "images" ]; then
  imagebuild_debian_check
  imagebuild_routines
  imagebuild_cleanup
else
  fullbuild_debian_check
  fullbuild_pre
  fullbuild_customise_config
  fullbuild
fi
