# plaw-openwrt-builds

A CI for my openwrt builds

Note: proxy support should only be used for local build testing, as it relies on
insecure http protocol. Final builds should NOT use proxy, thus ensuring
upstream https usage.


Todo:
* Logfile for output and errors
* Move build calls from this script into CI config
* Check release update automatically (parse arch packages directory, scan date column )
* Deployment
* CI pre-script check for targets and packages in upstream before attempting a build
* py3 stub creates DB of packages from upstream, triggers rebuild when needed packages update
* love and understanding between everyone


