#!/bin/sh

# TODO: Fix Open vs Password
# TODO: Rate limit

uci batch << EOF
add network switch_vlan
set network.@switch_vlan[-1].device='switch0'
set network.@switch_vlan[-1].ports='1t 5t'
set network.@switch_vlan[-1].vlan='5'

set network.guest='interface'
set network.guest.type='bridge'
set network.guest.ipaddr='192.168.17.1'
set network.guest.ifname='eth0.5'
set network.guest.netmask='255.255.255.0'
set network.guest.proto='static'
set network.guest.metric='9000'

set dhcp.guest='dhcp'
set dhcp.guest.interface='guest'
set dhcp.guest.leasetime='2h'
set dhcp.guest.limit='12'
set dhcp.guest.ra_management='1'
set dhcp.guest.start='16'

add wireless wifi-iface
set wireless.@wifi-iface[-1].ssid='Guests'
set wireless.@wifi-iface[-1].device='radio0'
set wireless.@wifi-iface[-1].mode='ap'
#set wireless.@wifi-iface[-1].encryption='psk2+ccmp'
set wireless.@wifi-iface[-1].encryption='open'
#set wireless.@wifi-iface[-1].key='example'
set wireless.@wifi-iface[-1].wps_pushbutton='0'
set wireless.@wifi-iface[-1].network='guest'
set wireless.@wifi-iface[-1].isolate='1'
set wireless.@wifi-iface[-1].wpa_disable_eapol_key_retries='1'

add wireless wifi-iface
set wireless.@wifi-iface[-1].ssid='Guests'
set wireless.@wifi-iface[-1].device='radio1'
set wireless.@wifi-iface[-1].mode='ap'
#set wireless.@wifi-iface[-1].encryption='psk2+ccmp'
set wireless.@wifi-iface[-1].encryption='open'
#set wireless.@wifi-iface[-1].key='example'
set wireless.@wifi-iface[-1].wps_pushbutton='0'
set wireless.@wifi-iface[-1].network='guest'
set wireless.@wifi-iface[-1].isolate='1'
set wireless.@wifi-iface[-1].wpa_disable_eapol_key_retries='1'

add firewall zone
set firewall.@zone[-1].name='guest'
set firewall.@zone[-1].network='guest'
set firewall.@zone[-1].output='ACCEPT'
set firewall.@zone[-1].input='REJECT'
set firewall.@zone[-1].forward='ACCEPT'

add firewall forwarding
set firewall.@forwarding[-1].dest='wan'
set firewall.@forwarding[-1].src='guest'

add firewall rule
set firewall.@rule[-1].target='ACCEPT'
set firewall.@rule[-1].proto='tcp udp'
set firewall.@rule[-1].dest_port='53'
set firewall.@rule[-1].name='Allow-DNS'
set firewall.@rule[-1].src='guest'

add firewall rule
set firewall.@rule[-1].target='ACCEPT'
set firewall.@rule[-1].dest_port='67'
set firewall.@rule[-1].name='Allow-DHCP-Client'
set firewall.@rule[-1].src='guest'
set firewall.@rule[-1].proto='udp'

commit
EOF
