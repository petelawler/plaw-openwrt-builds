#!/bin/sh

# AGPL licence here

# TODO: Set AP mode, password/open

uci batch << EOF
add wireless wifi-iface
set wireless.@wifi-iface[-1].device='radio0'
set wireless.@wifi-iface[-1].mode='ap'
set wireless.@wifi-iface[-1].ssid='TestNetwork'
set wireless.@wifi-iface[-1].network='wan'
set wireless.@wifi-iface[-1].isolate='1'
set wireless.@wifi-iface[-1].encryption='psk2+ccmp'
set wireless.@wifi-iface[-1].key='acompletelyreasonablepasswordforthetestwifi'
set wireless.@wifi-iface[-1].wpa_disable_eapol_key_retries='1'

add wireless wifi-iface
set wireless.@wifi-iface[-1].device='radio1'
set wireless.@wifi-iface[-1].mode='ap'
set wireless.@wifi-iface[-1].ssid='TestNetwork'
set wireless.@wifi-iface[-1].network='wan'
set wireless.@wifi-iface[-1].isolate='1'
set wireless.@wifi-iface[-1].encryption='psk2+ccmp'
set wireless.@wifi-iface[-1].key='acompletelyreasonablepasswordforthetestwifi'
set wireless.@wifi-iface[-1].wpa_disable_eapol_key_retries='1'

commit
EOF
