#!/bin/sh


#AGPLv3 licence here

# set -e
set -o pipefail
# set -u
IFS=$'\n\t'

init () {

    # TODO: Ensure we only run on OpenWrt machines

    if [ -z $1 ]; then
        echo "Must provide SSID as ARG1"
        exit 1
    fi
    SSID=$1
    # TODO: Work on Open WAP that have no password
    PSK=$2

    echo "Removing prior settings"
    if [ "$(uci -q show wireless.default_radio0)" ]; then
        uci delete wireless.default_radio0
    fi
    if [ "$(uci -q show wireless.default_radio1)" ]; then
        uci delete wireless.default_radio1
    fi
    # TODO: Remove prior instances of all our configs

    uci -q del_list firewall.@zone[1].network='wifi5wan'
    uci -q del_list firewall.@zone[1].network='wifi2wan'
    uci -q del_list firewall.@zone[1].network='usb0wan'

    uci -q del network.wifi2wan
    uci -q del network.wifi5wan
    uci -q del network.usb0wan

#    uci -q del wireless.wifi-iface='wifi2wan'
#    uci -q del wireless.wifi-iface='wifi5wan'

    uci commit


}


wifi2wan () {

    echo "Creating wifi2wan"

#   TODO: Name of the interface
    /sbin/uci batch << EOF

set network.wifi2wan='interface'
set network.wifi2wan.proto='dhcp'
add_list firewall.@zone[1].network='wifi2wan'

set wireless.radio0.disabled='1'
set wireless.radio0.channel='auto'
set wireless.radio0.country='AU'
set wireless.radio0.legacy_rates='1'

add wireless wifi-iface
set wireless.@wifi-iface[-1].ifname='wifi2wan'
set wireless.@wifi-iface[-1].network='wifi2wan'
set wireless.@wifi-iface[-1].ssid='$SSID'
set wireless.@wifi-iface[-1].encryption='psk2'
set wireless.@wifi-iface[-1].device='radio1'
set wireless.@wifi-iface[-1].mode='sta'
set wireless.@wifi-iface[-1].key='$PSK'

commit
EOF

}

wifi5wan () {

    echo "Creating wifi5wan"

#   TODO: Name of the interface
    /sbin/uci batch << EOF

set network.wifi5wan='interface'
set network.wifi5wan.proto='dhcp'
add_list firewall.@zone[1].network='wifi5wan'

set wireless.radio1.disabled='1'
set wireless.radio1.channel='auto'
set wireless.radio1.country='AU'
set wireless.radio1.legacy_rates='1'

add wireless wifi-iface
set wireless.@wifi-iface[-1].ifname='wifi5wan'
set wireless.@wifi-iface[-1].network='wifi5wan'
set wireless.@wifi-iface[-1].ssid='$SSID'
set wireless.@wifi-iface[-1].encryption='psk2'
set wireless.@wifi-iface[-1].device='radio0'
set wireless.@wifi-iface[-1].mode='sta'
set wireless.@wifi-iface[-1].key='$PSK'

commit
EOF

}


usbwan () {

# https://community.netgear.com/t5/Mobile-Routers-Hotspots-Modems/Nighthawk-M1-Linux-RNDIS-USB-Connectivity-Issues-After-Latest/m-p/1515552
# https://community.netgear.com/t5/Mobile-Routers-Hotspots-Modems/Netgear-810-update/m-p/1251948

    echo "Creating usb0wan"

    /sbin/uci batch << EOF

add_list firewall.@zone[1].network='usb0wan'

set network.usb0wan=interface
set network.usb0wan.network='usb0'
set network.usb0wan.proto='dhcp'
set network.usb0wan.ifname='usb0'

commit
EOF

}

enable_wifi () {

    echo "Enabling wifi"

    uci batch << EOF

set wireless.radio0.disabled='0'
set wireless.radio1.disabled='0'

commit
EOF
}

mwan3_configure_wifi2 () {

    echo "Configuring mwan3_wifi2"

    /sbin/uci batch << EOF
set mwan3.wifi2wan=interface
set mwan3.wifi2wan.enabled='0'
add_list mwan3.wifi2wan.track_ip='8.8.8.8'
add_list mwan3.wifi2wan.track_ip='208.67.220.220'
set mwan3.wifi2wan.family='ipv4'
set mwan3.wifi2wan.reliability='1'
set mwan3.wifi2wan.count='1'
set mwan3.wifi2wan.timeout='2'
set mwan3.wifi2wan.interval='5'
set mwan3.wifi2wan.down='3'
set mwan3.wifi2wan.up='8'

set mwan3.wifi2wan_m1_w2=member
set mwan3.wifi2wan_m1_w2.interface='wifi2wan'
set mwan3.wifi2wan_m1_w2.metric='1'
set mwan3.wifi2wan_m1_w2.weight='2'
set mwan3.wifi2wan_m2_w2=member
set mwan3.wifi2wan_m2_w2.interface='wifi2wan'
set mwan3.wifi2wan_m2_w2.metric='2'
set mwan3.wifi2wan_m2_w2.weight='2'
set mwan3.wifi2wan_only=policy
set mwan3.wifi2wan_only.use_member='wifi2wan_m1_w2'

set mwan3.wifi2wan_only=policy
set mwan3.wifi2wan_only.use_member='wifi2wan_m1_w2'

add_list mwan3.balanced.use_member='wifi2wan_m1_w2'

set mwan3.wan_wifi2wan=policy
add_list mwan3.wan_wifi2wan.use_member='wan_m1_w3'
add_list mwan3.wan_wifi2wan.use_member='wifi2wan_m2_w2'

set mwan3.wifi2wan_wan=policy
add_list mwan3.wifi2wan_wan.use_member='wan_m2_w3'
add_list mwan3.wifi2wan_wan.use_member='wifi2wan_m1_w2'

commit
EOF

}

mwan3_configure_wifi5 () {

    echo "Configuring mwan3_wifi5"

    /sbin/uci batch << EOF
set mwan3.wifi5wan=interface
set mwan3.wifi5wan.enabled='0'
add_list mwan3.wifi5wan.track_ip='8.8.8.8'
add_list mwan3.wifi5wan.track_ip='208.67.220.220'
set mwan3.wifi5wan.family='ipv4'
set mwan3.wifi5wan.reliability='1'
set mwan3.wifi5wan.count='1'
set mwan3.wifi5wan.timeout='2'
set mwan3.wifi5wan.interval='5'
set mwan3.wifi5wan.down='3'
set mwan3.wifi5wan.up='8'
set mwan3.wifi5wan_m1_w2=member
set mwan3.wifi5wan_m1_w2.interface='wifi5wan'
set mwan3.wifi5wan_m1_w2.metric='1'
set mwan3.wifi5wan_m1_w2.weight='2'
set mwan3.wifi5wan_m2_w2=member
set mwan3.wifi5wan_m2_w2.interface='wifi5wan'
set mwan3.wifi5wan_m2_w2.metric='2'
set mwan3.wifi5wan_m2_w2.weight='2'

set mwan3.wifi5wan_only=policy
set mwan3.wifi5wan_only.use_member='wifi5wan_m1_w2'

add_list mwan3.balanced.use_member='wifi5wan_m1_w2'

set mwan3.wan_wifi5wan=policy
add_list mwan3.wan_wifi5wan.use_member='wan_m1_w3'
add_list mwan3.wan_wifi5wan.use_member='wifi5wan_m2_w2'

set mwan3.wifi5wan_wan=policy
add_list mwan3.wifi5wan_wan.use_member='wan_m2_w3'
add_list mwan3.wifi5wan_wan.use_member='wifi5wan_m1_w2'

commit
EOF

}

mwan3_configure_usb0 () {

    echo "Configuring mwan3_usb0"

    /sbin/uci batch << EOF
set mwan3.usb0wan=interface
set mwan3.usb0wan.enabled='0'
add_list mwan3.usb0wan.track_ip='8.8.8.8'
add_list mwan3.usb0wan.track_ip='208.67.220.220'
set mwan3.usb0wan.family='ipv4'
set mwan3.usb0wan.reliability='1'
set mwan3.usb0wan.count='1'
set mwan3.usb0wan.timeout='2'
set mwan3.usb0wan.interval='5'
set mwan3.usb0wan.down='3'
set mwan3.usb0wan.up='8'
set mwan3.usb0wan_m1_w2=member
set mwan3.usb0wan_m1_w2.interface='usb0wan'
set mwan3.usb0wan_m1_w2.metric='1'
set mwan3.usb0wan_m1_w2.weight='2'
set mwan3.usb0wan_m2_w2=member
set mwan3.usb0wan_m2_w2.interface='usb0wan'
set mwan3.usb0wan_m2_w2.metric='2'
set mwan3.usb0wan_m2_w2.weight='2'

set mwan3.usb0wan_only=policy
set mwan3.usb0wan_only.use_member='usb0wan_m1_w2'

add_list mwan3.balanced.use_member='usb0wan_m1_w2'

set mwan3.wan_usb0wan=policy
add_list mwan3.wan_usb0wan.use_member='wan_m1_w3'
add_list mwan3.wan_usb0wan.use_member='usb0wan_m2_w2'

set mwan3.usb0wan_wan=policy
add_list mwan3.usb0wan_wan.use_member='wan_m2_w3'
add_list mwan3.usb0wan_wan.use_member='usb0wan_m1_w2'

commit
EOF

}


init "$1" "$2"

wifi2wan
mwan3_configure_wifi2
wifi5wan
mwan3_configure_wifi5
enable_wifi
if [ -n "$(/sbin/lsmod | /usr/bin/grep usbnet)" ]; then
    usbwan
    mwan3_configure_usb0
fi

echo "restarting firewall"
/etc/init.d/firewall restart > /dev/null 2>&1
echo "restarting network"
/etc/init.d/network restart
#/etc/init.d/mwan3 restart
