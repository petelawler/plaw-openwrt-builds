#!/bin/bash

# openwrt-openvpn-server-setup.sh
# Create an OpenWrt OpenVPN server with some client config files
# Copyright (C) Peter Lawler <relwalretep@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Based upon https://openwrt.org/docs/guide-user/services/vpn/openvpn/basic

# TODO: Custom server device name
# TODO: Whether to remove existing key/certfiles

set -e
set -o pipefail
set -u
IFS=$'\n\t'

chk_opkg () {

    LC_ALL=C $OPKG list-installed | $AWK '{print $1}' | $GREP "^${1}"  || OWRT_PKGS="${OWRT_PKGS}${1} "

}

chk_opkg_prereq () {

    $ECHO "Checking for pre-requisite packages"
    for pkg in ${OPKG_PREREQ_PACKAGES[*]}; do
        # $ECHO "Testing $pkg"
        chk_opkg $pkg
    done

    if [ -n "$OWRT_PKGS" ] ; then
        $ECHO "Need packages: $OWRT_PKGS"
        $ECHO "Updating OpenWrt package cache"
        $OPKG update
        $ECHO "Checking and installing needed packages"
        $OPKG install "$OPKGS"
    else
        $ECHO "Package check passed"
    fi

}


init () {

    /bin/echo "Initialising"
# Basics
    AWK=$(command -v awk)  > /dev/null 2>&1
    CAT=$(command -v cat)  > /dev/null 2>&1
    CHMOD=$(command -v chmod)  > /dev/null 2>&1
    CP=$(command -v cp)  > /dev/null 2>&1
    ECHO=$(command -v echo)  > /dev/null 2>&1
    GREP=$(command -v grep)  > /dev/null 2>&1
    HEAD=$(command -v head)  > /dev/null 2>&1
    LS=$(command -v ls)  > /dev/null 2>&1
    MKDIR=$(command -v mkdir)  > /dev/null 2>&1
    OPENSSL=$(command -v openssl)  > /dev/null 2>&1
    RM=$(command -v rm)  > /dev/null 2>&1
    SED=$(command -v sed)  > /dev/null 2>&1
    TOUCH=$(command -v touch)  > /dev/null 2>&1

# Platform dependent
    OPKG=$(command -v opkg)  > /dev/null 2>&1
    UCI=$(command -v uci)  > /dev/null 2>&1

# Switches
    VERBOSE_SWITCH="--verbose"

# Script requirements
    OWRT_PKGS=""
    OPKG_PREREQ_PACKAGES=(openvpn-openssl luci-app-openvpn)
    chk_opkg_prereq

# See where OpenVPN got installed
    OPENVPN=$(command -v openvpn)  > /dev/null 2>&1

    echo "Loading functions"
#   Obtaining server address from WAN-interface IP
    set +u
    source /lib/functions/network.sh
    network_find_wan WAN_IF
    network_get_ipaddrs WAN_SERVER_ADDR "$WAN_IF"
    set -u


    echo "Setting defaults"
    OVPNSERVER_DIR="/etc/openvpn"

    DH_KEY_SIZE_DEFAULT="2048"
    DH_KEY_SIZE=$DH_KEY_SIZE_DEFAULT
    PRIVATE_KEY_SIZE_DEFAULT="2048"
    PRIVATE_KEY_SIZE=$PRIVATE_KEY_SIZE_DEFAULT

    OVPNSERVER_PORT_DEFAULT="1194"
    OVPNSERVER_PORT=""
    OVPNSERVER_PROTO_DEFAULT="udp"
    OVPNSERVER_PROTO=""
    OVPNCLIENT_DEV_DEFAULT="tun"
    OVPNCLIENT_DEV=""
    OVPNCLIENT_COMPRESSION_DEFAULT="lzo"
    OVPNCLIENT_COMPRESSION=""
    OVPNSERVER_LOCAL_NETWORK_DEFAULT="10.10.10.0"
    OVPNSERVER_LOCAL_NETWORK_NETMASK_DEFAULT="255.255.255.0"
    OVPNSERVER_LOCAL_NETWORK_DNS_DEFAULT="10.10.10.1"

    SERVER_HOSTNAME="$(uci -q get system.@system[0].hostname)"
    OVPNSERVER_NAME_DEFAULT=$SERVER_HOSTNAME
    OVPNSERVER_NAME=""
# TODO: Fix if ddns is installed but not enabled
#    if [ -n "$(uci -q show ddns)" ]; then
#        OVPNSERVER_NAME_DEFAULT="$(uci -q get $(uci -q show ddns | sed -n -e "1s/^\(.*\)\.enabled='1'$/\1/p").lookup_host)"
# TODO: Fix lookup of WAN port
#    else [ -z "$(nslookup $WAN_SERVER_ADDR | grep Name\: | awk '{print $2}')" ]
#        OVPNSERVER_NAME="$(nslookup $WAN_SERVER_ADDR | grep Name\: | awk '{print $2}')"
#    fi
#   Setting configuration parameters
    if [ -n "$(uci -q get openvpn.$OVPNSERVER_NAME_DEFAULT)" ]; then
        OVPNSERVER_PORT="$(uci -q get openvpn.$OVPNSERVER_NAME_DEFAULT.port)"
        OVPNSERVER_PROTO="$(uci -q get openvpn.$OVPNSERVER_NAME_DEFAULT.proto)"
        OVPNCLIENT_DEV="$(uci -q get openvpn.$OVPNSERVER_NAME_DEFAULT.dev | sed -e "s/\d*$//")"
        OVPNCLIENT_COMPRESSION="$(uci -q get openvpn.$OVPNSERVER_NAME_DEFAULT.compress)"
    fi

# TODO: improve display of current settings / default entry

    read -p "$OVPNSERVER_NAME_DEFAULT OpenVPN server name: $OVPNSERVER_PORT " OVPNSERVER_NAME
    # TODO: Set a sensible uci config name based upon legal values of this name
    OVPNSERVER_NAME=${OVPNSERVER_NAME:-$OVPNSERVER_NAME_DEFAULT}
    OVPNSERVER_UCI_NAME=$(echo "$OVPNSERVER_NAME" | sed 's/[^a-zA-Z0-9]//g')
    OVPNSERVER_UCI_NAME="ovpn${OVPNSERVER_UCI_NAME:0:10}"
    $ECHO "$OVPNSERVER_NAME ($OVPNSERVER_UCI_NAME)"

    read -p "$OVPNSERVER_NAME OpenVPN server port: $OVPNSERVER_PORT " OVPNSERVER_PORT
    OVPNSERVER_PORT=${OVPNSERVER_PORT:-$OVPNSERVER_PORT_DEFAULT}
    $ECHO $OVPNSERVER_PORT

    read -p "$OVPNSERVER_NAME OpenVPN server protocol: $OVPNSERVER_PROTO" OVPNSERVER_PROTO
    OVPNSERVER_PROTO=${OVPNSERVER_PROTO:-$OVPNSERVER_PROTO_DEFAULT}
    $ECHO $OVPNSERVER_PROTO

    read -p "$OVPNSERVER_NAME OpenVPN client device: $OVPNCLIENT_DEV" OVPNCLIENT_DEV
    OVPNCLIENT_DEV=${OVPNCLIENT_DEV:-$OVPNCLIENT_DEV_DEFAULT}
    $ECHO $OVPNCLIENT_DEV

    read -p "$OVPNSERVER_NAME compression: $OVPNCLIENT_COMPRESSION" OVPNCLIENT_COMPRESSION
    OVPNCLIENT_COMPRESSION=${OVPNCLIENT_COMPRESSION:-$OVPNCLIENT_COMPRESSION_DEFAULT}
    $ECHO $OVPNCLIENT_COMPRESSION

# TODO: Default networks
    read -p "OpenVPN Local network: " OVPNSERVER_LOCAL_NETWORK
    OVPNSERVER_LOCAL_NETWORK=${OVPNSERVER_LOCAL_NETWORK:-$OVPNSERVER_LOCAL_NETWORK_DEFAULT}
    $ECHO $OVPNSERVER_LOCAL_NETWORK

    read -p "OpenVPN Local netmask: " OVPNSERVER_LOCAL_NETWORK_NETMASK
    OVPNSERVER_LOCAL_NETWORK_NETMASK=${OVPNSERVER_LOCAL_NETWORK_NETMASK:-$OVPNSERVER_LOCAL_NETWORK_NETMASK_DEFAULT}
    $ECHO $OVPNSERVER_LOCAL_NETWORK_NETMASK

    read -p "OpenVPN Local network DNS: " OVPNSERVER_LOCAL_NETWORK_DNS
    OVPNSERVER_LOCAL_NETWORK_DNS=${OVPNSERVER_LOCAL_NETWORK_DNS:-$OVPNSERVER_LOCAL_NETWORK_DNS_DEFAULT}
    $ECHO $OVPNSERVER_LOCAL_NETWORK_DNS

    OVPNSERVER_NAME_DIR="$OVPNSERVER_DIR/$OVPNSERVER_NAME"
    PKI_DIR="$OVPNSERVER_DIR/ssl-$OVPNSERVER_NAME"
    PKI_CONF="$PKI_DIR/openssl.cnf"

    OVPN_CLIENTS=""
#    OVPN_CLIENTS=()

    $ECHO "OVPNSERVER_NAME: $OVPNSERVER_NAME"
    $ECHO "OVPNSERVER_NAME_DIR: $OVPNSERVER_NAME_DIR"
    $ECHO "OVPNSERVER_PORT: $OVPNSERVER_PORT"
    $ECHO "OVPNSERVER_PROTO: $OVPNSERVER_PROTO"
    $ECHO "OVPNSERVER_LOCAL_NETWORK $OVPNSERVER_LOCAL_NETWORK"
    $ECHO "OVPNSERVER_LOCAL_NETWORK_NETMASK: $OVPNSERVER_LOCAL_NETWORK_NETMASK"
    $ECHO "OVPNSERVER_LOCAL_NETWORK_DNS: $OVPNSERVER_LOCAL_NETWORK_DNS"

    $ECHO "OVPNCLIENT_DEV: $OVPNCLIENT_DEV"
    $ECHO "OVPNCLIENT_COMPRESSION: $OVPNCLIENT_COMPRESSION"


    $ECHO "Initialisation complete"
}

ovpn_server_config () {

    if [ -d "$OVPNSERVER_NAME_DIR" ]
    then
        echo "Removing $OVPNSERVER_NAME_DIR"
        $RM --force --recursive $VERBOSE_SWITCH "$OVPNSERVER_NAME_DIR"
    fi
        if [ -d "$PKI_DIR" ]
    then
        echo "Removing $PKI_DIR"
        $RM --force --recursive $VERBOSE_SWITCH "$PKI_DIR"
    fi

    $ECHO "Force file permissions on $OVPNSERVER_DIR"
    $CHMOD --recursive $VERBOSE_SWITCH 0700 "$OVPNSERVER_DIR"
    $ECHO "Creating Directory Structure"

    $MKDIR --parents $VERBOSE_SWITCH "$PKI_DIR" "$OVPNSERVER_NAME_DIR"
    $CHMOD --recursive $VERBOSE_SWITCH 0700 "$PKI_DIR" "$OVPNSERVER_NAME_DIR"
    $TOUCH $PKI_DIR/index.txt $PKI_DIR/index
    $CHMOD $VERBOSE_SWITCH 600 $PKI_DIR/index.txt $PKI_DIR/index
    $ECHO 1000 > $PKI_DIR/serial
    $CHMOD $VERBOSE_SWITCH 600 $PKI_DIR/serial
    $CP --interactive $VERBOSE_SWITCH /etc/ssl/openssl.cnf "$PKI_DIR"

    $ECHO "Customizing openssl.cnf"

    $SED -i "
                              s:\\\\:/:g
/^dir/                        s:=.*:= $PKI_DIR:
/^new_certs_dir/              s:=.*:= $PKI_DIR:
/.*Name/                      s:= match:= optional:
/organizationName_default/    s:= .*:= VLA:
/stateOrProvinceName_default/ s:= .*:= Hobart:
/countryName_default/         s:= .*:= AU:
/default_days/                s:=.*:= 90:
/default_bits/                s:=.*:= $PRIVATE_KEY_SIZE:
" $PKI_CONF

    $CAT << EOF >> "$PKI_CONF"
[ $OVPNSERVER_NAME ]
  keyUsage = digitalSignature, keyEncipherment
  extendedKeyUsage = serverAuth
EOF

    for client in ${OVPN_CLIENTS[*]}; do
        $CAT << EOF >> "$PKI_CONF"
[ $client ]
  keyUsage = digitalSignature
  extendedKeyUsage = clientAuth
EOF
    done

}

ovpn_server_gen_authorities () {

    $ECHO "Generating Server PSK and CA, Server, & Client Certs"

    $ECHO "Generating $OVPNSERVER_NAME PSK"
    $OPENVPN --genkey --secret "$PKI_DIR/tc.psk"
    $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/tc.psk

    $ECHO "Generating $OVPNSERVER_NAME Authority Cert & Key"
    $OPENSSL req -batch -nodes -new -keyout "$PKI_DIR/ca.key" \
        -out "$PKI_DIR/ca.crt" -x509 -config "$PKI_CONF" -days "90" && \
        $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/ca.key

}

ovpn_server_gen_cert_key () {

    $ECHO "Generating $OVPNSERVER_NAME Cert & Key"
    $OPENSSL req -batch -nodes -new -keyout "$PKI_DIR/$OVPNSERVER_NAME.key" \
        -out "$PKI_DIR/$OVPNSERVER_NAME.csr" -subj "/CN=$OVPNSERVER_NAME" \
        -config "$PKI_CONF" && \
        $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/$OVPNSERVER_NAME.key $PKI_DIR/$OVPNSERVER_NAME.csr

    $ECHO "Signing $OVPNSERVER_NAME Cert"
    $OPENSSL ca  -batch -keyfile "$PKI_DIR/ca.key" -cert "$PKI_DIR/ca.crt" \
        -in "$PKI_DIR/$OVPNSERVER_NAME.csr" -out "$PKI_DIR/$OVPNSERVER_NAME.crt" \
        -config "$PKI_CONF" -extensions "$OVPNSERVER_NAME" && \
        $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/ca.crt $PKI_DIR/$OVPNSERVER_NAME.crt

}

ovpn_server_gen_psk () {

    $ECHO "Generating OpenVPN TLS PSK"
    $OPENVPN --genkey --secret "$PKI_DIR/tls-auth.key"
    $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/tls-auth.key

}

ovpn_server_gen_dh_cert () {

    $ECHO "Generating Diffie-Hellman Cert"
# May take a while to complete
    $OPENSSL dhparam -out "$PKI_DIR/dh$DH_KEY_SIZE.pem" $DH_KEY_SIZE && \
        $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/dh$DH_KEY_SIZE.pem

}

ovpn_server_install_files () {

    $ECHO "Copying Certs & Keys to $OVPNSERVER_NAME_DIR"
    $CP $VERBOSE_SWITCH $PKI_DIR/ca.crt $PKI_DIR/$OVPNSERVER_NAME.* $PKI_DIR/dh$DH_KEY_SIZE.pem $PKI_DIR/tc.psk $PKI_DIR/tls-auth.key $OVPNSERVER_NAME_DIR

}

ovpn_server_backup_files () {

    $ECHO "Backing up to $BACKUP_DIR"
    $CP --recursive $VERBOSE_SWITCH $PKI_DIR $BACKUP_DIR

}

uci_server_setup () {

# Code based upon https://openwrt.org/_export/code/docs/guide-user/services/vpn/openvpn/server.setup?codeblock=2

    $ECHO "Setting up network.$OVPNSERVER_UCI_NAME"
$UCI batch << EOF
set network.$OVPNSERVER_UCI_NAME="interface"
set network.$OVPNSERVER_UCI_NAME.proto="none"
set network.$OVPNSERVER_UCI_NAME.ifname="tun0"
set network.$OVPNSERVER_UCI_NAME.auto="1"
commit network
EOF

    $ECHO "Establishing firewall for $OVPNSERVER_UCI_NAME"
$UCI batch << EOF
add firewall rule
set firewall.@rule[-1].name="Allow-OpenVPN-$OVPNSERVER_UCI_NAME"
set firewall.@rule[-1].src="wan"
set firewall.@rule[-1].proto="tcp udp"
set firewall.@rule[-1].dest_port="$OVPNSERVER_PORT"
set firewall.@rule[-1].target="ACCEPT"

add firewall zone
set firewall.@zone[-1].name="$OVPNSERVER_UCI_NAME"
add_list firewall.@zone[-1].network="$OVPNSERVER_UCI_NAME"
set firewall.@zone[-1].input="ACCEPT"
set firewall.@zone[-1].output="ACCEPT"
set firewall.@zone[-1].forward="REJECT"

add firewall forwarding
set firewall.@forwarding[-1].src="$OVPNSERVER_UCI_NAME"
set firewall.@forwarding[-1].dest="wan"

add firewall forwarding
set firewall.@forwarding[-1].src="$OVPNSERVER_UCI_NAME"
set firewall.@forwarding[-1].dest="lan"
commit firewall
EOF

    $ECHO "Settings up openvpn.$OVPNSERVER_UCI_NAME"
$UCI batch << EOF
set openvpn.$OVPNSERVER_UCI_NAME="openvpn"
set openvpn.$OVPNSERVER_UCI_NAME.enabled="1"
set openvpn.$OVPNSERVER_UCI_NAME.verb="3"
set openvpn.$OVPNSERVER_UCI_NAME.dev="tun0"
set openvpn.$OVPNSERVER_UCI_NAME.topology="subnet"
set openvpn.$OVPNSERVER_UCI_NAME.proto="udp"
set openvpn.$OVPNSERVER_UCI_NAME.port="$OVPNSERVER_PORT"
set openvpn.$OVPNSERVER_UCI_NAME.server="$OVPNSERVER_LOCAL_NETWORK $OVPNSERVER_LOCAL_NETWORK_NETMASK"
set openvpn.$OVPNSERVER_UCI_NAME.client_to_client="1"
set openvpn.$OVPNSERVER_UCI_NAME.compress="lzo"
set openvpn.$OVPNSERVER_UCI_NAME.keepalive="10 120"
set openvpn.$OVPNSERVER_UCI_NAME.persist_tun="1"
set openvpn.$OVPNSERVER_UCI_NAME.persist_key="1"
set openvpn.$OVPNSERVER_UCI_NAME.ifconfig-pool-persist="/tmp/$OVPNSERVER_NAME-ipp.txt"
set openvpn.$OVPNSERVER_UCI_NAME.tls_crypt="$OVPNSERVER_NAME_DIR/tc.psk"
set openvpn.$OVPNSERVER_UCI_NAME.dh="$OVPNSERVER_NAME_DIR/dh$DH_KEY_SIZE.pem"
set openvpn.$OVPNSERVER_UCI_NAME.ca="$OVPNSERVER_NAME_DIR/ca.crt"
set openvpn.$OVPNSERVER_UCI_NAME.cert="$OVPNSERVER_NAME_DIR/$OVPNSERVER_UCI_NAME.crt"
set openvpn.$OVPNSERVER_UCI_NAME.key="$OVPNSERVER_NAME_DIR/$OVPNSERVER_UCI_NAME.key"
set openvpn.$OVPNSERVER_UCI_NAME.log=/tmp/openvpn-log-$OVPNSERVER_UCI_NAME.log'
set openvpn.$OVPNSERVER_UCI_NAME.status '/tmp/openvpn-status-$OVPNSERVER_UCI_NAME.log'

add_list openvpn.$OVPNSERVER_UCI_NAME.push="redirect-gateway def1"
add_list openvpn.$OVPNSERVER_UCI_NAME.push="route $OVPNSERVER_LOCAL_NETWORK $OVPNSERVER_LOCAL_NETWORK_NETMASK"
add_list openvpn.$OVPNSERVER_UCI_NAME.push="dhcp-option DNS $OVPNSERVER_LOCAL_NETWORK_DNS"
add_list openvpn.$OVPNSERVER_UCI_NAME.push="compress lzo"
add_list openvpn.$OVPNSERVER_UCI_NAME.push="persist-tun"
add_list openvpn.$OVPNSERVER_UCI_NAME.push="persist-key"
EOF

    OVPNSERVER_DOMAIN="$(uci -q get dhcp.@dnsmasq[0].domain)"
    if [ -n "$OVPNSERVER_DOMAIN" ]; then
        $UCI add_list openvpn.$OVPNSERVER_UCI_NAME.push="dhcp-option DOMAIN $OVPNSERVER_DOMAIN"
    fi
    $UCI commit openvpn

}

ovpn_client_gen_cert_key () {
    $ECHO "Generating Client Certs & Keys"
    local __client_id
    __client_id=$1
    $ECHO "Generating $__client_id Cert & Key"
    $OPENSSL req -batch -nodes -new -keyout "$PKI_DIR/$__client_id.key" \
        -out "$PKI_DIR/$__client_id.csr" -subj "/CN=$__client_id" -config "$PKI_CONF"
    $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/$__client_id.key $PKI_DIR/$__client_id.csr
    $ECHO "Signing $__client_id Cert"
    $OPENSSL ca  -batch -keyfile "$PKI_DIR/ca.key" -cert "$PKI_DIR/ca.crt" \
        -in "$PKI_DIR/$__client_id.csr" -out "$PKI_DIR/$__client_id.crt" \
        -config "$PKI_CONF" -extensions "$__client_id"
    $CHMOD $VERBOSE_SWITCH 0600 "$PKI_DIR/$__client_id.key"
    $ECHO "Generating $__client_id.p12"
    $OPENSSL pkcs12 -export \
        -in "$PKI_DIR/$__client_id.crt" \
        -inkey "$PKI_DIR/$__client_id.key" \
        -out "$PKI_DIR/$__client_id.p12" \
        -passout pass:
    $CHMOD $VERBOSE_SWITCH 0600 "$PKI_DIR/$__client_id.key" $PKI_DIR/$__client_id.p12
    $CP $VERBOSE_SWITCH $PKI_DIR/$__client_id.* $OVPNSERVER_NAME_DIR
    $ECHO "Completed $__client_id"

}

ovpn_client_create_certs () {

# based on https://openwrt.org/_export/code/docs/guide-user/services/vpn/openvpn/basic?codeblock=6
    local __client_id
    __client_id=$1
    CLIENT_CONF="$OVPNSERVER_NAME_DIR/$__client_id.ovpn"
    if [ -f $OVPNSERVER_NAME_DIR/tc.psk ]; then
        TC_KEY="$(sed -e "/^#/d" "$OVPNSERVER_NAME_DIR/tc.psk")"
    else
        $ECHO "Unable to access $OVPNSERVER_NAME_DIR/tc.psk"
        exit 1
    fi
    if [ -f $OVPNSERVER_NAME_DIR/ca.crt ]; then
        CA_CERT="$($OPENSSL x509 -in "$OVPNSERVER_NAME_DIR/ca.crt")"
    else
        $ECHO "Unable to access $OVPNSERVER_NAME_DIR/ca.crt"
        exit 1
    fi
    $ECHO "Generating $__client_id Cert & Key"
    $OPENSSL req -batch -nodes -new -keyout "$PKI_DIR/$__client_id.key" \
        -out "$PKI_DIR/$__client_id.csr" -subj "/CN=$__client_id" -config "$PKI_CONF"
    $CHMOD $VERBOSE_SWITCH 0600 $PKI_DIR/$__client_id.key $PKI_DIR/$__client_id.csr
    $ECHO "Signing $__client_id Cert"
    $OPENSSL ca  -batch -keyfile "$PKI_DIR/ca.key" -cert "$PKI_DIR/ca.crt" \
        -in "$PKI_DIR/$__client_id.csr" -out "$PKI_DIR/$__client_id.crt" \
        -config "$PKI_CONF" -extensions "$__client_id"
    $CHMOD $VERBOSE_SWITCH 0600 "$PKI_DIR/$__client_id.key"
    $ECHO "Generating $__client_id.p12"
    $OPENSSL pkcs12 -export \
        -in "$PKI_DIR/$__client_id.crt" \
        -inkey "$PKI_DIR/$__client_id.key" \
        -out "$PKI_DIR/$__client_id.p12" \
        -passout pass:
    $CHMOD $VERBOSE_SWITCH 0600 "$PKI_DIR/$__client_id.key" $PKI_DIR/$__client_id.p12
    $CP $VERBOSE_SWITCH $PKI_DIR/$__client_id.* $OVPNSERVER_NAME_DIR
    if [ -f $OVPNSERVER_NAME_DIR/$__client_id.crt ] ;then
        CLIENT_CERT="$($OPENSSL x509 -in "$OVPNSERVER_NAME_DIR/$__client_id.crt")"
    else
        $ECHO "Unable to access $OVPNSERVER_NAME_DIR/$__client_id.crt"
        exit 1
    fi
    if [ -f $OVPNSERVER_NAME_DIR/$__client_id.key ]; then
        CLIENT_KEY="$($OPENSSL pkcs8 -in "$OVPNSERVER_NAME_DIR/$__client_id.key" -nocrypt)"
    else
        $ECHO "Unable to access $OVPNSERVER_NAME_DIR/$__client_id.key"
        exit 1
    fi
    cat << EOF >> "$CLIENT_CONF"
verb 3
nobind
dev $OVPNCLIENT_DEV
client
remote $WAN_SERVER_ADDR $OVPNSERVER_PORT $OVPNSERVER_PROTO
fast-io
compress $OVPNCLIENT_COMPRESSION
remote-cert-tls server
<tls-crypt>
$TC_KEY
</tls-crypt>
<ca>
$CA_CERT
</ca>
<cert>
$CLIENT_CERT
</cert>
<key>
$CLIENT_KEY
</key>
EOF
            $ECHO "Created $CLIENT_ID.ovpn"
}

ovpn_create_client_configs () {

    if [ -z $OVPN_CLIENTS ]; then
        CLIENT_COUNT=1
        $GREP -l -e "TLS Web Client Authentication" "$OVPNSERVER_NAME_DIR"/*.crt \
            | $SED -e "s/^.*\///;s/\.[^.]*$//" \
            | while $ECHO "Enter client name $CLIENT_COUNT" ; read CLIENT_ID
            do
                ovpn_client_create_certs $CLIENT_ID
                $CLIENT_NUM=$(($CLIENT_NUM+1))
            done
    else
        for CLIENT_ID in ${OVPN_CLIENTS[*]}; do
            ovpn_client_create_certs $CLIENT_ID
        done
    fi

    for file in $OVPNSERVER_NAME_DIR/*.ovpn; do
        $CHMOD 600 $file
        $HEAD -v -n 0 $file
    done

}

init

ovpn_server_config
ovpn_server_gen_authorities
ovpn_server_gen_cert_key
ovpn_server_gen_psk
ovpn_server_gen_dh_cert
ovpn_server_install_files
uci_server_setup

ovpn_create_client_configs

/etc/init.d/openvpn enable
/etc/init.d/network reload
/etc/init.d/firewall reload
/etc/init.d/openvpn reload
