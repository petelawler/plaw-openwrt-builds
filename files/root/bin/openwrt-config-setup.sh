#!/bin/sh

#AGPLv3 licence here

# set -e
set -o pipefail
# set -u
IFS=$'\n\t'

. /lib/functions.sh

persistent_log () {
    echo "Setting persistent log"

    /sbin/uci batch << EOF

set system.log_file='/mnt/sda3/log/messages'
set system.log_type='file'

commit
EOF

}