#!/bin/sh
# Insert rule for forwarding established connection traffic, just before # the final rule (reject)

# https://openwrt.org/docs/guide-user/firewall/fw3_configurations/fw3_parent_controls

new_rule_num=$(iptables -v -L FORWARD --line-numbers | grep reject | cut
-d ' ' -f 1)
iptables -I FORWARD $new_rule_num -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Delete first rule for forwarding established connection traffic
old_rule_num=$(iptables -v -L FORWARD --line-numbers | grep ESTABLISHED | cut -d ' ' -f 1 | sed -n 1p)
iptables -D FORWARD $old_rule_num
